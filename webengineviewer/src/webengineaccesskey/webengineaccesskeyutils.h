/*
   SPDX-FileCopyrightText: 2016-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/
#pragma once

#include <QString>

namespace WebEngineViewer
{
namespace WebEngineAccessKeyUtils
{
Q_REQUIRED_RESULT QString script();
}
}
